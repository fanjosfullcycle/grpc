FROM golang

COPY entrypoint.sh /go/

RUN chmod +x /go/entrypoint.sh

WORKDIR /go/app/src/

RUN apt update

ENTRYPOINT ["/go/entrypoint.sh"]

# #RUN touch /go/bin/entrypoint.sh
# RUN touch /entrypoint.sh

# RUN chmod +x /entrypoint.sh

# RUN echo '#!/bin/bash'
# RUN echo 'go mod init grpc' >> /entrypoint.sh
# RUN echo 'go get google.golang.org/grpc' >> /entrypoint.sh

# WORKDIR /go/src/

# RUN apt-get update
# #RUN go mod init grpc
# #RUN go get google.golang.org/grpc
# RUN go get google.golang.org/protobuf/cmd/protoc-gen-go@latest
# RUN go install google.golang.org/protobuf/cmd/protoc-gen-go@latest

# RUN apt install protobuf-compiler -y

# RUN go get google.golang.org/protobuf/cmd/protoc-gen-go google.golang.org/grpc/cmd/protoc-gen-go-grpc

# RUN  echo 'PATH="/go/bin:$PATH"' >> /etc/bash.bashrc

# #ENTRYPOINT ["entrypoint.sh"]
# #precisa rodar esses comandos apos subir
# # go mod init grpc
# # go get google.golang.org/grpc
# # go get github.com/codeedu/fc2-grpc/pb
# # go install github.com/ktr0731/evans@latest
# # evans -r repl --host localhost --port 50051