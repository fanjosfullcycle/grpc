#!/bin/bash
export PATH="$PATH:$(go env GOPATH)/bin"
go mod init github.com/test/test
go get google.golang.org/protobuf/cmd/protoc-gen-go@v1.26
go get google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.1
go get github.com/codeedu/fc2-grpc/pb
go get github.com/codeedu/fc2-grpc/services
go get google.golang.org/grpc
go get google.golang.org/grpc/reflection
go run /go/app/src/server/server.go